// App config the for development environment.
// Do not require this directly. Use ./src/config instead.

export default {

  // apiRoot: "https://api.500px.com/v1",

  // 500px consumer key
  apiKey: "onscreen201531415926",

  apiRoot: "localhost:3000/api/v1",

  // apiKey: "onscreentodaycommediaart",

  // Supported locales
  locales: ["eng", "chn", "chnt"]

}
