// App config the for production environment.
// Do not require this directly. Use ./src/config instead.

export default {
  // 500px consumer key
  apiKey: "onscreen201531415926",

  // Google Analytics tracking id (skipped on dev)
  trackingId: "UA-62091544-1",

  apiRoot: "localhost:3117/api/v1",

  // Supported locales
  locales: ["eng", "chn", "chnt"]

}
